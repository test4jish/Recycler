
/**
 * Project          : Recycler
 * Module           : Clustering and Bootstrapping
 * Source filename  : app.js
 * Description      :  Cluster and fork to number of CPU's available and bootstrap modules.
 * Author           : Prasanth P <prasanth.p@robosoftin.com>
 * Copyright        : Copyright © 2017
 *                    
 */


var env = process.env.NODE_ENV || 'development';
var config = require('./Configs/config')[env];
var autoIncrement = require('mongoose-auto-increment');
// Load modules

var express = require('express');
var http = require('http');
var morgan = require('morgan');
var useragent = require('express-useragent');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var fs = require('fs');
var app = express();


app.use(useragent.express());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Paths
var routesPath = config.root + '/Routes';
var modelsPath = config.root + '/Models';


// Connect to MongoDB
mongoose.connect(config.dbURL);
var db = mongoose.connection;
autoIncrement.initialize(db);

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log("Database connection to MongoDB opened.");
});

// Bootstrap models
fs.readdirSync(modelsPath).forEach(function (file) {
    if (file.substr(file.lastIndexOf('.') + 1) !== 'js') {
        return;
    }
    console.log("Loading model " + file);
    require(modelsPath + '/' + file);
});

//Cross Domain
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', true);
    if (req.headers.origin !== undefined) {
        res.header("Access-Control-Allow-Origin", req.headers.origin);
    } else {
        res.header("Access-Control-Allow-Origin", "*");
    }
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Accept-Encoding');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,PATCH');
    //res.header('Access-Control-Max-Age', '3000');

    if (req.method == 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});


app.use(morgan(':method :url :status :response-time ms - :res[content-length] :req[headers]'));
app.all('*', function (req, res, next) {
    console.log('---------------------------------------------------------------------------');
    console.log('%s %s on %s from ', req.method, req.url, new Date(), req.useragent.source);
    console.log('Request Headers: ', req.headers);
    console.log('Request Body: ', req.body);
    console.log('Request Files: ', req.files);
    next();
});


// Bootstrap routes
fs.readdirSync(routesPath).forEach(function (file) {
    if (file.substr(file.lastIndexOf('.') + 1) !== 'js') {
        return;
    }
    console.log("Loading route " + file);
    require(routesPath + '/' + file)(app, config);
});

app.listen(config.port, function () {
    console.log('Server waiting for connections...',config.port);
});

module.exports = app;